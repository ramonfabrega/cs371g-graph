// -------
// Graph.h
// --------

#ifndef Graph_h
#define Graph_h

// --------
// includes
// --------
#include <iostream>
#include <cassert> // assert
#include <cstddef> // size_t
#include <utility> // make_pair, pair
#include <vector>  // vector

// -----
// Graph
// -----

class Graph
{
public:
    // ------
    // usings
    // ------

    using vertex_descriptor = int;

    struct Edge
    {
        vertex_descriptor source = -1;
        vertex_descriptor target = -1;

        Edge() {};
        Edge(const vertex_descriptor &source, const vertex_descriptor &target)
        {
            this->source = source;
            this->target = target;
        }

        void operator=(const Edge &other)
        {
            source = other.source;
            target = other.target;
        }

        bool operator==(const Edge &rhs) const
        {
            return (source == rhs.source && target == rhs.target);
        }

        bool operator!=(const Edge &rhs) const
        {
            return !(*this == rhs);
        }

        friend std::ostream &operator<<(std::ostream &os, const Edge &e);
    };

    using edge_descriptor = Edge;

    using vertices_size_type = std::size_t;
    using edges_size_type = std::size_t;

    using vertex_iterator = std::vector<vertex_descriptor>::const_iterator;
    using edge_iterator = std::vector<edge_descriptor>::const_iterator;
    using adjacency_iterator = std::vector<vertex_descriptor>::const_iterator;

public:
    // --------
    // add_edge
    // --------

    /**
     * add edge to graph from vertex descriptors
     */
    friend std::pair<edge_descriptor, bool> add_edge(vertex_descriptor s, vertex_descriptor t, Graph &g)
    {
        // edge exists already in edgelist
        for (const auto &edge : g.edge_list)
        {
            if (edge.source == s && edge.target == t) // found target
                return std::make_pair(Edge(s, t), false);
        }

        // add edge
        Edge new_edge(s, t);
        g.edge_list.push_back(new_edge); // added to graph

        // handle adjacency of s/t vertices
        try
        {
            g.adjacency_matrix.at(s).push_back(t);
        }
        catch (const std::out_of_range &e)
        {
            g.adjacency_matrix.push_back(std::vector<vertex_descriptor>());
            g.adjacency_matrix.at(s).push_back(t);
        }

        g.num_edges++; // edge counter

        return std::make_pair(new_edge, true);
    }

    // ----------
    // add_vertex
    // ----------

    /**
     * add vertex to graph
     * vertex_descriptor value based on current global vertice index
     */
    friend vertex_descriptor add_vertex(Graph &g)
    {
        vertex_descriptor v = g.curr_index_vert;                        // vd value is >= 0 increasing
        g.adjacency_matrix.push_back(std::vector<vertex_descriptor>()); // add to adjacency matrix
        g.vertex_descriptor_list.push_back(v);                          // add to vertex list

        g.num_verts++;       // vert counter
        g.curr_index_vert++; // vert index

        return v;
    }

    // -----------------
    // adjacent_vertices
    // -----------------

    /**
     * adjacent vertices of given vertex description of graph
     * returns an std::pair of adjacency iterators for begin, end
     * utlizies 2d container of graph class
     */
    friend std::pair<adjacency_iterator, adjacency_iterator> adjacent_vertices(vertex_descriptor v, const Graph &g)
    {
        return std::make_pair(g.adjacency_matrix[v].begin(), g.adjacency_matrix[v].end());
    }

    // ----
    // edge
    // ----

    /**
     * edge function checking existence of edge for given s/t in a graph
     * returns an std::pair similar to add_edge
     */
    friend std::pair<edge_descriptor, bool> edge(vertex_descriptor s, vertex_descriptor t, const Graph &g)
    {
        Edge new_edge(s, t);               // struct init
        for (const auto &it : g.edge_list) // for each loop
        {
            if (it.source == s && it.target == t)
                return std::make_pair(Edge(s, t), true);
        }

        return std::make_pair(Edge(s, t), false);
    }

    // -----
    // edges
    // -----

    /**
     * returns std::pair of edge iterators from begin to end
     * uses graph's edge_list data structure
     */
    friend std::pair<edge_iterator, edge_iterator> edges(const Graph &g)
    {
        return std::make_pair(g.edge_list.begin(), g.edge_list.end());
    }

    // ---------
    // num_edges
    // ---------

    /**
     * returns counter from graph private data 'num_edges'
     */
    friend edges_size_type num_edges(const Graph &g)
    {
        return g.num_edges;
    }

    // ------------
    // num_vertices
    // ------------

    /**
     * returns counter from graph private data 'num_verts'
     */
    friend vertices_size_type num_vertices(const Graph &g)
    {
        return g.num_verts;
    }

    // ------
    // source
    // ------

    /**
     * returns value from edge private data 'source'
     */
    friend vertex_descriptor source(edge_descriptor edge, const Graph &)
    {
        return edge.source;
    }

    // ------
    // target
    // ------

    /**
     * returns value from edge private data 'target'
     */
    friend vertex_descriptor target(edge_descriptor edge, const Graph &)
    {
        return edge.target;
    }

    // ------
    // vertex
    // ------

    /**
     * vertex_descriptor id is the same
     */
    friend vertex_descriptor vertex(vertices_size_type i, const Graph &)
    {
        vertex_descriptor vd = i;
        return i;
    }

    // --------
    // vertices
    // --------

    /**
     * returns std::pair of vertex_iterators from graph private data list
     */
    friend std::pair<vertex_iterator, vertex_iterator> vertices(const Graph &g)
    {
        return std::make_pair(g.vertex_descriptor_list.begin(), g.vertex_descriptor_list.end());
    }

private:
    // ----
    // data
    // ----

    std::vector<vertex_descriptor> vertex_descriptor_list;        // holds vertex_descriptor's
    std::vector<std::vector<vertex_descriptor>> adjacency_matrix; // 2d holds vector's of vertex_descriptor's
    std::vector<edge_descriptor> edge_list;                       // holds edge_descriptor's which build on Edge struct

    vertices_size_type num_verts = 0;       // vert counter
    edges_size_type num_edges = 0;          // edge counter
    vertices_size_type curr_index_vert = 0; // index

    // -----
    // valid
    // -----

    /**
     * check if data struct is valid
     */
    bool valid() const
    {
        return (num_verts >= 0 && num_edges >= 0 && curr_index_vert >= 0);
    }

public:
    // --------
    // defaults
    // --------

    Graph() = default;
    Graph(const Graph &) = default;
    ~Graph() = default;
    Graph &operator=(const Graph &) = default;
};

std::ostream &operator<<(std::ostream &os, const Graph::Edge &e)
{
    os << "(" << e.source << "," << e.target << ")";
    return os;
}

#endif // Graph_h